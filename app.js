const express = require('express');
require('dotenv').config({ path: `${__dirname}/.env` })
require('./database/connection')

const app = express()


app.use(express.json())


module.exports = app