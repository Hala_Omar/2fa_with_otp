const { connect } = require('mongoose');

const connection = connect(process.env.MONGODB_URL , {
    useNewUrlParser : true , 
    useUnifiedTopology : true 
} , ( err , connection) =>{
    err ? console.error(err) :
    connection ? console.log("DataBase Connected Successfulley"): ""
})

module.exports = connection