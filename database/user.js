const { Schema , model } = require('mongoose');
const jwt = require('jsonwebtoken');
const connection = require('./connection')


const userSchema = new Schema ( {
    phoneNumber : {
        type : String , 
        required : true
    } , 
    jwt : {
        type : String , 
    }
} , { timestamps : true }) //create Time and date

userSchema.methods.generateJwt = function(){
    const token = jwt.sign(
        { _id : this._id , 
          phoneNumber : this.phoneNumber 
        } ,
        process.env.JWT_SECRET_KEY ,
        {expiresIn : "7d"}
    )
    this.jwt = token

}

module.exports.User = model('otpuser' , userSchema)